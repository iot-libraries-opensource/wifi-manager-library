#include "WiFiManager.h"

WiFiManagerLibrary::WiFiManagerLibrary()
{
    this->connected = false;
}

WiFiManagerLibrary::~WiFiManagerLibrary()
{

}

void WiFiManagerLibrary::begin(AsyncWebServer *httpServer)
{
    EepromStorage.begin(); // moze sie powtarzac - jest zabezpieczone

    if (!EepromStorage.exists("wifi-ssid")) {
        IPAddress accessPointIp(1, 2, 3, 4);
        WiFi.softAPConfig(accessPointIp, accessPointIp, IPAddress(255, 255, 255, 0));
        WiFi.softAP(AP_SSID, AP_PASSWORD);
        
        Serial.print("AP ssid:\t");
        Serial.println(AP_SSID);
        Serial.print("AP password:\t");
        Serial.println(AP_PASSWORD);
        Serial.print("IP address:\t");
        Serial.println(WiFi.softAPIP());

        httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", index_html);
        });

        httpServer->on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
            if(!request->hasParam("ssid", true) || !request->hasParam("password", true)) {
                return request->send(400, "text/html", "Both parameters are needed.");
            }

            String ssid = request->getParam("ssid", true)->value();
            String password = request->getParam("password", true)->value();
            
            EepromStorage.set<String>("wifi-ssid", ssid);
            EepromStorage.set<String>("wifi-pass", password);
            EepromStorage.apply();

            resetItself();
        });
    } else {
        String ssid = EepromStorage.get<String>("wifi-ssid");
        String pass = EepromStorage.get<String>("wifi-pass");

        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, pass);

        if (WiFi.waitForConnectResult() != WL_CONNECTED) {
            Serial.printf("WiFi Failed!\n");
            return;
        }

        this->connected = true;

        Serial.print("IP Address: ");
        Serial.println(WiFi.localIP());
    }
}

void WiFiManagerLibrary::clear() 
{
    EepromStorage.remove("wifi-ssid");
    EepromStorage.remove("wifi-pass");
}

bool WiFiManagerLibrary::isConnected()
{
    return this->connected;
}