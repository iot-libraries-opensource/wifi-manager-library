#ifndef __WIFI_MANAGER_H__
#define __WIFI_MANAGER_H__

#include <Arduino.h>
#include <EepromRepository.h>
#include <string.h>
#include "../view/view.h"
#include "../include/consts.h"
#include "../include/helpers.h"

#ifndef EEPROM_SIZE
  #if defined(ESP8266) || defined(ESP32) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    #define EEPROM_SIZE 4096
  #endif
  #if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega32U4__)
    #define EEPROM_SIZE 1024
  #endif
  #if defined(__AVR_ATmega168__)
    #define EEPROM_SIZE 512
  #endif
#endif

class WiFiManagerLibrary
{
    AsyncWebServer *httpServer;
    bool connected;

public:
    WiFiManagerLibrary();
    ~WiFiManagerLibrary();
    void begin(AsyncWebServer *httpServer);
    void clear();
    bool isConnected();
};

static WiFiManagerLibrary WiFiManager;
extern WiFiManagerLibrary WiFiManager;

#endif