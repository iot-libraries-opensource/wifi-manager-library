#ifndef __HELPERS_H__
#define __HELPERS_H__

// auto restart
inline void(* resetItself) (void) = 0;

#endif